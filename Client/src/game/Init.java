package game;

public class Init {
    public static void main(String[] args){
        Logger.WRITE("Client init", "Launching client . . .");
        Canvas canvas = new Canvas();
        canvas.main("game.Canvas");
    }
}
