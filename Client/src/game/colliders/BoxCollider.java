package game.colliders;

import processing.core.PApplet;

public class BoxCollider extends Collider {
    float xs;
    float ys;
    public BoxCollider(float x, float y, float xs, float ys){
        super(x, y, 1);
        this.xs = xs;
        this.ys = ys;
    }

    @Override
    public boolean isColliding(Collider checkObj) {
        switch(checkObj.typeId) {
            case 0:
                if (true) {
                    return true;
                } else {
                    return false;
                }
            case 1:
                boolean colliding = false;
                float bx = ((BoxCollider)checkObj).x;
                float bxs = ((BoxCollider)checkObj).xs;
                float by = ((BoxCollider)checkObj).y;
                float bys = ((BoxCollider)checkObj).ys;
                if(contains(bx, x, x + xs) || contains(bx + bxs, x, x + xs) || (bx < x && bx + bxs > x)) {
                    if(contains(by, y, y + ys) || contains(by + bys, y, y + ys)){
                        colliding = true;
                    }else if(by < y && by + bys > y){
                        colliding = true;
                    }
                }
                return colliding;
        }
        return false;
    }

    @Override
    public void drawCollider(PApplet canvas) {
        canvas.strokeWeight(1);
        canvas.noFill();
        canvas.rect(x, y, xs, ys);
    }
}
