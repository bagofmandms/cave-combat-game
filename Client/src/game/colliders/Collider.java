package game.colliders;

import processing.core.PApplet;

public abstract class Collider {
    float x;
    float y;
    int typeId;
    public Collider(float x, float y, int typeId){
        this.x = x;
        this.y = y;
        this.typeId = typeId;
    }
    public void setXY(float x, float y){
        this.x = x;
        this.y = y;
    }
    protected boolean contains(float val, float a, float b){
        return ((val < a && val > b)||(val > a && val < b));
    }
    protected float dist(float x1, float y1, float x2, float y2){
        return (float)Math.sqrt((y1 - y2) * (y1 - y2) + (x1 - x2) * (x1 - x2));
    }
    public abstract boolean isColliding(Collider checkObj);
    public abstract void drawCollider(PApplet canvas);
}
