package game.colliders;

import processing.core.PApplet;

public class CircleCollider extends Collider {
    float r;
    public CircleCollider(float x, float y, float r){
        super(x, y, 0);
        this.r = r;
    }

    @Override
    public boolean isColliding(Collider checkObj) {
        switch(checkObj.typeId){
            case 0:
                if(dist(x, y, checkObj.x, checkObj.y) < ((CircleCollider)checkObj).r + r){
                    return true;
                }else{
                    return false;
                }
            case 1:
                float bx = ((BoxCollider)checkObj).x;
                float bxs = ((BoxCollider)checkObj).xs;
                float by = ((BoxCollider)checkObj).y;
                float bys = ((BoxCollider)checkObj).ys;
                float cx = x;
                float cy = y;
                float cr = r;
                if(contains(cx, bx, bx + bxs)){
                    if(contains(cy, by - cr, by + bys + cr)){
                        return true;
                    }
                }else if(contains(cy, by, by + bys)){
                    if(contains(cx, bx - cr, bx + bxs + cr)){
                        return true;
                    }
                }else{
                    if(cx < bx){
                        if(cy < by){
                            return dist(cx, cy, bx, by) < cr;
                        }else{
                            return dist(cx, cy, bx, by + bys) < cr;
                        }
                    }else{
                        if(cy < by){
                            return dist(cx, cy, bx + bxs, by) < cr;
                        }else{
                            return dist(cx, cy, bx + bxs, by + bys) < cr;
                        }
                    }
                }
        }
        return false;
    }

    @Override
    public void drawCollider(PApplet canvas) {
        canvas.strokeWeight(1);
        canvas.noFill();
        canvas.circle(x, y, r * 2);
    }
}
