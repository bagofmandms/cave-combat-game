package game;

import java.io.*;
import java.util.HashMap;

public class Serializer {
    public static final Serializer SERIALIZER = new Serializer();
    private static FileOutputStream writer;
    private static BufferedReader reader;
    private static HashMap<String, String> storedData;
    public Serializer(){
        storedData = new HashMap<>();
        LOADDATA(".");
        try{
            writer = new FileOutputStream("." + "/serial.txt", true);
        }catch (Exception e) {
            System.out.println("Serializer failed during initialization: " + e);
        }
        STOREDATA();

    }
    private static void LOADDATA(String location){
        try {
            File log = new File(location + "/serial.txt");
            log.createNewFile();
            reader = new BufferedReader(new FileReader(location + "/serial.txt"));
            String line = "";
            while ((line = reader.readLine()) != null) {
                storedData.put(line.split(":")[0], line.split(":")[1]);
            }
        } catch (Exception e){
            System.out.println("Serializer failed during reading: " + e);
        }
    }
    public static void PUTDATA(String name, String data){
        storedData.put(name, data);
        STOREDATA();
    }
    public static void STOREDATA(){
        try {
            PrintWriter preWriter = new PrintWriter("." + "/serial.txt");
            preWriter.print("");
            preWriter.close();
            for(HashMap.Entry<String, String> entry : storedData.entrySet()) {
                writer.write((entry.getKey() + ":" + entry.getValue() + "\n").getBytes());
            }
        }catch(Exception e){
            System.out.println("Serializer failed during writing: " + e);
        }
    }

    public static String GETDATA(String key, String defValue){
        if(storedData.containsKey(key)){
            return storedData.get(key);
        }
        Serializer.PUTDATA(key, defValue);
        return defValue;
    }
}
