package game.interactables;

import game.Canvas;

public abstract class Button extends UIElement {
    boolean pressed = false;
    protected float x, y, w, h;
    public Button(int x, int y, int w, int h){
        this.x = x * UIEventManager.scale;
        this.y = y * UIEventManager.scale;
        this.w = w * UIEventManager.scale;
        this.h = h * UIEventManager.scale;
    }
    public void mouseEvent(int mx, int my, int code, Canvas cv){
        if(code == 0 && contains(mx, my)){
            pressed = true;
        }else if(code == 1){
            if(contains(mx, my) && pressed) {
                onPress();
            }
            pressed = false;
        }

    }
    public void render(Canvas cv){
        if(pressed && contains(cv.mouseX, cv.mouseY)){
            renderPressed(cv);
        }else{
            renderNormal(cv);
        }
    }

    @Override
    public void keyEvent(Canvas cv) { }

    protected abstract void renderNormal(Canvas cv);
    protected abstract void renderPressed(Canvas cv);
    protected abstract void onPress();
    private boolean contains(int x, int y){
        return (this.x < x && x < this.x + w)&&(this.y < y && y < this.y + h);
    }
}
