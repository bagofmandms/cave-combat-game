package game.interactables;

public class UIEvent {
    enum EVENTTYPE {
        DEBUG,
        KEY,
        BUTTON
    }
    public int type;
    public int code;
    public UIEvent(){

    }
}
