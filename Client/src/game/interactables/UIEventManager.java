package game.interactables;

import game.Canvas;
import game.renders.Renderer;

import java.util.ArrayList;

public class UIEventManager {
    public static ArrayList<UIElement> elements = new ArrayList<>();
    public static Renderer activeRenderer;
    public static float scale;
    public static void findScale(Canvas cv){
        scale = cv.width / 1680.0f;
    }
    public static void setActiveRenderer(Renderer activeRendererInput){
        clearElements();
        activeRenderer = activeRendererInput;
        activeRenderer.activate();
    }
    public static void render(Canvas cv){
        activeRenderer.render(cv);
    }
    public static void renderElements(Canvas cv){
        for(UIElement element : elements){
            element.render(cv);
        }
    }
    public static void mouseEvent(int code, Canvas cv){
        for(int i = 0; i < elements.size(); i++){
            elements.get(i).mouseEvent(cv.mouseX, cv.mouseY, code, cv);
        }
    }
    public static void keyEvent(Canvas cv){
        for(int i = 0; i < elements.size(); i++){
            elements.get(i).keyEvent(cv);
        }
    }
    public static void clearElements(){
        elements = new ArrayList<>();
    }
}
