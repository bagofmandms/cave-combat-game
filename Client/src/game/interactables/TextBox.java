package game.interactables;

import game.Canvas;

public abstract class TextBox extends UIElement {

    public String content = "";
    boolean active = false;
    protected float x, y, w, h;
    public TextBox(int x, int y, int w, int h){
        this.x = x * UIEventManager.scale;
        this.y = y * UIEventManager.scale;
        this.w = w * UIEventManager.scale;
        this.h = h * UIEventManager.scale;
    }
    public TextBox(int x, int y, int w, int h, String content){
        this.x = x * UIEventManager.scale;
        this.y = y * UIEventManager.scale;
        this.w = w * UIEventManager.scale;
        this.h = h * UIEventManager.scale;
        this.content = content;
    }

    @Override
    public void render(Canvas cv) {
        cv.fill(255, 255, 255);
        if(active) {
            cv.stroke(100, 255, 100);
        }else{
            cv.stroke(100, 100, 100);
        }
        cv.rect(x, y, w, h);
        cv.fill(0, 0, 0);
        cv.text(content, (x + (w / 2.0f)), (y + (h / 2.0f)) - 6); // 6 is the offset that is eyeballed for the Ebrima font since it renders low for an unknown reason
    }

    @Override
    public void mouseEvent(int x, int y, int code, Canvas cv) {
        if(contains(x, y) && code == 2){
            active = true;
        }else if(active && code == 1 && !contains(x, y)){
            active = false;
            sendEvent();
        }
    }

    @Override
    public void keyEvent(Canvas cv) {
        if(active) {
            if (("" + cv.key).toUpperCase().charAt(0) >= 65 && ("" + cv.key).toUpperCase().charAt(0) <= 90) {
                content += ("" + cv.key).toUpperCase();
            } else if (("" + cv.key).charAt(0) >= 46 && ("" + cv.key).charAt(0) <= 57) {
                content += ("" + cv.key);
            } else if (cv.key == '.') {
                content += ("" + cv.key);
            } else if (cv.keyCode == cv.BACKSPACE) {
                if(content.length() > 0){
                    content = content.substring(0, content.length() - 1);
                }
            } else if (cv.keyCode == cv.ENTER || cv.keyCode == cv.RETURN || cv.keyCode == cv.ESC) {
                active = false;
                sendEvent();
            }
        }
    }

    public abstract void sendEvent();
    private boolean contains(int x, int y){
        return (this.x < x && x < this.x + w)&&(this.y < y && y < this.y + h);
    }
}
