package game.interactables;

import game.Canvas;

public abstract class UIElement {
    public abstract void render(Canvas cv);
    public abstract void mouseEvent(int x, int y, int code, Canvas cv);
    public abstract void keyEvent(Canvas cv);
}
