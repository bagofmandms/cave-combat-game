package game.interactables;

import game.Canvas;
import processing.core.PApplet;

public abstract class Slider extends UIElement {
    float x, y, w, h;
    protected float pos;
    float size;
    boolean pressed;

    public Slider(int x, int y, int w, int h, float size, float pos){
        this.x = x * UIEventManager.scale;
        this.y = y * UIEventManager.scale;
        this.w = w * UIEventManager.scale;
        this.h = h * UIEventManager.scale;
        this.pos = pos;
        this.size = size;
    }

    @Override
    public void render(Canvas cv) {
        if(pressed){
           pos = Math.min(Math.max((cv.mouseX - (x + h / 2))/(w - h), 0), 1);
        }
        cv.stroke(0, 0, 0);
        cv.fill(255, 255, 255);
        cv.strokeWeight(size / 5);
        cv.rect(x, y, w, h);
        cv.line(x + h / 2, y + h / 2, x + w - h / 2, y + h / 2);
        cv.circle(x + h / 2 + (w - h) * pos, y + h / 2, size / 2);

    }

    @Override
    public void mouseEvent(int x, int y, int code, Canvas cv) {
        if(code == 0 && PApplet.dist(this.x + h / 2 + (w - h) * pos, this.y + h / 2, cv.mouseX, cv.mouseY) < size / 2){
            pressed = true;
        }else if(code == 1){
            if(pressed){
                onUpdate();
            }
            pressed = false;
        }
    }

    @Override
    public void keyEvent(Canvas cv) {

    }

    public abstract void onUpdate();
}
