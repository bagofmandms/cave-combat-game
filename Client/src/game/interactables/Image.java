package game.interactables;

import game.Canvas;
import game.resourcemanagement.ResourceManager;

public class Image extends UIElement{
    private ResourceManager.TEXTURES tex;
    protected float x, y;
    public Image(int x, int y, ResourceManager.TEXTURES tex){
        this.x = x * UIEventManager.scale;
        this.y = y * UIEventManager.scale;
        this.tex = tex;
    }

    @Override
    public void render(Canvas cv) {
        cv.image(ResourceManager.images.get(tex.ordinal()), x, y);
    }

    @Override
    public void mouseEvent(int x, int y, int code, Canvas cv) {

    }

    @Override
    public void keyEvent(Canvas cv) {

    }
}
