package game.interactables;

import game.Canvas;

public abstract class DynamicText extends UIElement {
    float x, y, w, h;
    int r, g, b;
    public String content;
    public DynamicText(float x, float y, float w, float h, int r, int g, int b){
        this.x = x * UIEventManager.scale;
        this.y = y * UIEventManager.scale;
        this.w = w * UIEventManager.scale;
        this.h = h * UIEventManager.scale;
        this.r = r;
        this.g = g;
        this.b = b;
        retrieveContent();
    }

    public abstract void retrieveContent();

    public abstract void retrieveColor();

    @Override
    public void render(Canvas cv) {
        cv.fill(r, g, b);
        cv.text(content, (x + (w / 2.0f)), (y + (h / 2.0f)) - 6); // 6 is the offset that is eyeballed for the Ebrima font since it renders low for an unknown reason
    }

    @Override
    public void mouseEvent(int x, int y, int code, Canvas cv) {
        retrieveContent();
        retrieveColor();
    }

    @Override
    public void keyEvent(Canvas cv) {
        retrieveContent();
        retrieveColor();
    }
}
