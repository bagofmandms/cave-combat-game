package game.renders;

import game.Canvas;
import game.Init;
import game.Serializer;
import game.interactables.*;
import game.manager.Game;
import game.resourcemanagement.ResourceManager;

public class CreateRoomScreen extends Renderer {
    public static String size = "2";
    @Override
    public void render(Canvas cv) {
        cv.background(0);
        UIEventManager.renderElements(cv);
        cv.rect(450,50,800,200);
    }

    @Override
    public void activate() {
        UIEventManager.elements.add(new StaticText(650, 400, 100, 70, "PORT:",255,255,255));
        UIEventManager.elements.add(new StaticText(605, 525, 100, 70, "PASSWORD:",255,255,255));
        String port = Serializer.GETDATA("CreateRoomPort", "8080");
        String password = Serializer.GETDATA("CreateRoomPassword", "");
        size = Serializer.GETDATA("CreateRoomSize", "2");
        UIEventManager.elements.add(new TextBox(750,400,400,70, port) {
                @Override
                public void sendEvent() {
                    Serializer.PUTDATA("CreateRoomPort", content);
                }
            });

        UIEventManager.elements.add(new TextBox(750,525,400,70, password) {
            @Override
            public void sendEvent() {
                Serializer.PUTDATA("CreateRoomPassword", content);
            }
        });

        UIEventManager.elements.add(new Button(590,650,100,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.fill(200,200,200);
                cv.stroke(255);
                cv.rect(x, y, w, h);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.fill(100,100,100);
                cv.stroke(255);
                cv.rect(x, y, w, h);
            }

            @Override
            protected void onPress() {
                CreateRoomScreen.size = String.valueOf(Math.max(Integer.parseInt(CreateRoomScreen.size) - 1, 2));
                Serializer.PUTDATA("CreateRoomSize", CreateRoomScreen.size);
            }

        });
        UIEventManager.elements.add(new Button(990,650,100,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.fill(200,200,200);
                cv.stroke(255);
                cv.rect(x, y, w, h);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.fill(100,100,100);
                cv.stroke(255);
                cv.rect(x, y, w, h);
            }

            @Override
            protected void onPress() {
                CreateRoomScreen.size = String.valueOf(Math.min(Integer.parseInt(CreateRoomScreen.size) + 1, 16));
                Serializer.PUTDATA("CreateRoomSize", CreateRoomScreen.size);
            }

        });
        UIEventManager.elements.add(new StaticText(790,600,100,70, "size:", 255, 255, 255));
        UIEventManager.elements.add(new DynamicText(790, 650, 100, 70, 255, 255, 255) {
            @Override
            public void retrieveContent() {
                content = CreateRoomScreen.size;
            }

            @Override
            public void retrieveColor() {}
        });



        UIEventManager.elements.add(new Button(890,800,200,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.CREATE_ROOM_BUTTON), x, y);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.CREATE_ROOM_BUTTON_PRESSED), x, y);
            }

            @Override
            protected void onPress() {
                int port = Integer.parseInt(Serializer.GETDATA("CreateRoomPort", "8080").replaceAll("[^0-9]", ""));
                if(port < 65535 && port > 0){
                    Game.OpenServer(Integer.parseInt(size), port, Serializer.GETDATA("CreateRoomPassword", ""));
                    UIEventManager.setActiveRenderer(Renderers.LOBBY_SCREEN);
                    LobbyScreen.host = true;
                }else{

                }
            }

        });
        UIEventManager.elements.add(new Button(590,800,200,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.EXIT_BUTTON), x, y);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.EXIT_BUTTON_PRESSED), x, y);
            }

            @Override
            protected void onPress() {
                UIEventManager.setActiveRenderer(Renderers.TITLE_SCREEN);
            }

        });
    }
}
