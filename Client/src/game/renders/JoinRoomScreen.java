package game.renders;

import game.Canvas;
import game.Serializer;
import game.interactables.*;
import game.manager.Game;
import game.resourcemanagement.ResourceManager;

public class JoinRoomScreen extends Renderer {
    @Override
    public void render(Canvas cv) {
        cv.background(0);
        cv.stroke(0);
        UIEventManager.renderElements(cv);
    }

    @Override
    public void activate() {
        String JoinRoomIP = Serializer.GETDATA("JoinRoomIP", "");
        String JoinRoomPort = Serializer.GETDATA("JoinRoomPort", "");
        String JoinRoomPassword = Serializer.GETDATA("JoinRoomPassword", "");

        UIEventManager.elements.add(new TextBox(640,400,400,70, JoinRoomIP) {
            @Override
            public void sendEvent() {
                Serializer.PUTDATA("JoinRoomIP", content);
            }
        });

        UIEventManager.elements.add(new TextBox(640,500,400,70, JoinRoomPort) {
            @Override
            public void sendEvent() {
                Serializer.PUTDATA("JoinRoomPort", content);
            }
        });

        UIEventManager.elements.add(new TextBox(640,600,400,70, JoinRoomPassword) {
            @Override
            public void sendEvent() {
                Serializer.PUTDATA("JoinRoomPassword", content);
            }
        });

        UIEventManager.elements.add(new Button(890,700,200,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.JOIN_ROOM_BUTTON), x, y);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.JOIN_ROOM_BUTTON_PRESSED), x, y);
            }

            @Override
            protected void onPress() {
                String IP = Serializer.GETDATA("JoinRoomIP", "8080").replaceAll("[^0-9.]", "");
                int port = Integer.parseInt(Serializer.GETDATA("JoinRoomPort", "8080").replaceAll("[^0-9]", ""));
                Game.StartClient(IP, port, Serializer.GETDATA("JoinRoomPassword", ""));

            }

        });

        UIEventManager.elements.add(new Button(590,700,200,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.EXIT_BUTTON), x, y);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.EXIT_BUTTON_PRESSED), x, y);
            }

            @Override
            protected void onPress() {
                UIEventManager.setActiveRenderer(Renderers.TITLE_SCREEN);
            }

        });
    }
}
