package game.renders;

public class Renderers {
    public static final LoadingScreen LOADING_SCREEN = new LoadingScreen();
    public static final CreateRoomScreen CREATE_ROOM_SCREEN = new CreateRoomScreen();
    public static final JoinRoomScreen JOIN_ROOM_SCREEN = new JoinRoomScreen();
    public static final LobbyScreen LOBBY_SCREEN = new LobbyScreen();
    public static final TitleScreen TITLE_SCREEN = new TitleScreen();
    public static final SettingsScreen SETTINGS_SCREEN = new SettingsScreen();
}
