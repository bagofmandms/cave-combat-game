package game.renders;

import game.Canvas;
import game.Logger;
import game.Serializer;
import game.interactables.Button;
import game.interactables.Image;
import game.interactables.UIEvent;
import game.interactables.UIEventManager;
import game.manager.Game;
import game.resourcemanagement.ResourceManager;

public class TitleScreen extends Renderer {
    @Override
    public void render(Canvas cv) {
        cv.background(0);
        cv.stroke(0);
        cv.fill(255, 255, 255);
        cv.rect(50,50,800,200);
        cv.rect(100,400,300,450);
        cv.rect(1100,300,500,600);
        UIEventManager.renderElements(cv);
    }

    @Override
    public void activate() {
        UIEventManager.elements.add(new Image(0, 0, ResourceManager.TEXTURES.TEST));
        UIEventManager.elements.add(new Button(150,450,200,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.CREATE_ROOM_BUTTON), x, y);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.CREATE_ROOM_BUTTON_PRESSED), x, y);
            }

            @Override
            protected void onPress() {
                UIEventManager.setActiveRenderer(Renderers.CREATE_ROOM_SCREEN);
            }

        });

        UIEventManager.elements.add(new Button(150,550,200,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.JOIN_ROOM_BUTTON), x, y);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.JOIN_ROOM_BUTTON_PRESSED), x, y);
            }

            @Override
            protected void onPress() {
                UIEventManager.setActiveRenderer(Renderers.JOIN_ROOM_SCREEN);
            }

        });

        UIEventManager.elements.add(new Button(150,650,200,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.SETTINGS_BUTTON), x, y);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.SETTINGS_BUTTON_PRESSED), x, y);
            }

            @Override
            protected void onPress() {
                SettingsScreen.returnScreen = Renderers.TITLE_SCREEN;
                UIEventManager.setActiveRenderer(Renderers.SETTINGS_SCREEN);
            }

        });

        UIEventManager.elements.add(new Button(150,750,200,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.EXIT_BUTTON), x, y);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.image(ResourceManager.getImage(ResourceManager.TEXTURES.EXIT_BUTTON_PRESSED), x, y);
            }

            @Override
            protected void onPress() {
                Logger.WRITE("UI manager", "Exiting . . .");
                Serializer.STOREDATA();
                System.exit(0);
            }

        });
    }
}
