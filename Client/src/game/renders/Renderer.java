package game.renders;

import game.Canvas;

public abstract class Renderer {
    public abstract void render(Canvas cv);
    public abstract void activate();
}
