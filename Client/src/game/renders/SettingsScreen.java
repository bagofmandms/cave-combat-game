package game.renders;

import game.Canvas;
import game.Logger;
import game.Serializer;
import game.interactables.Button;
import game.interactables.Slider;
import game.interactables.StaticText;
import game.interactables.UIEventManager;
import game.resourcemanagement.ResourceManager;


public class SettingsScreen extends Renderer {
    public static Renderer returnScreen;
    @Override
    public void render(Canvas cv) {
        cv.background(0);
        UIEventManager.renderElements(cv);

    }

    @Override
    public void activate() {
        UIEventManager.elements.add(new StaticText(740, 200, 200, 70, "SETTINGS",255,255,255));
        UIEventManager.elements.add(new StaticText(440, 400, 200, 70, "SOUND:",255,255,255));
        UIEventManager.elements.add(new StaticText(440, 500, 200, 70, "MUSIC:",255,255,255));
        String a = Serializer.GETDATA("Sound", "1.0");
        String b = Serializer.GETDATA("Music", "1.0");
        float sound = Float.parseFloat(a);
        float music = Float.parseFloat(b);
        UIEventManager.elements.add(new Slider(640,400,600,70,40, sound){
            @Override
            public void onUpdate() {
                Serializer.PUTDATA("Sound", String.valueOf(pos));
            }
        });
        UIEventManager.elements.add(new Slider(640,500,600,70,40, music){
            @Override
            public void onUpdate() {
                Serializer.PUTDATA("Music", String.valueOf(pos));
            }
        });
        UIEventManager.elements.add(new Button(440,300,800,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.fill(100,100,100);
                cv.stroke(255);
                cv.rect(x, y, w, h);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.fill(0);
                cv.stroke(255);
                cv.rect(x, y, w, h);
            }

            @Override
            protected void onPress() {

            }

        });
        UIEventManager.elements.add(new Button(440,600,800,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.fill(100,100,100);
                cv.stroke(255);
                cv.rect(x, y, w, h);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.fill(0);
                cv.stroke(255);
                cv.rect(x, y, w, h);
            }

            @Override
            protected void onPress() {

            }

        });
        UIEventManager.elements.add(new Button(440,700,800,70){


            @Override
            protected void renderNormal(Canvas cv) {
                cv.fill(100,100,100);
                cv.stroke(255);
                cv.rect(x, y, w, h);
            }

            @Override
            protected void renderPressed(Canvas cv) {
                cv.fill(0);
                cv.stroke(255);
                cv.rect(x, y, w, h);
            }

            @Override
            protected void onPress() {
                UIEventManager.setActiveRenderer(returnScreen);
            }

        });
    }
}
