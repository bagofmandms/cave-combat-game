package game.renders;

import game.Canvas;
import game.interactables.Button;
import game.interactables.UIEvent;
import game.interactables.UIEventManager;
import game.manager.Game;

public class LoadingScreen extends Renderer {
    int n = 100;
    int r = 300;
    int v = 50;
    float progress = 0;
    int tick = 0;
    public void render(Canvas cv){
        cv.strokeWeight(5);
        cv.stroke(255, 255, 255);
        //cv.colorMode(cv.HSB, 1, 1, 1);
        tick++;
        progress = (progress + 0.05f) % 1.0f;
        cv.background(0);
        //cv.stroke(cv.sin(tick/(2 * cv.PI * 60)), 1, 1);
        for(int i = 0; i <= n; i++){
            float seg = 2 * cv.PI / n;
            float rc = cv.sin(tick/(2 * cv.PI * 10) * (i * seg)) * r;
            float rl = cv.sin(tick/(2 * cv.PI * 10) * ((i + 1) * seg)) * r;
            float x = cv.width/2 +   rc * cv.cos(seg * i);
            float y = cv.height/2 +  rc * cv.sin(seg * i);
            float lx = cv.width/2 +  rl * cv.cos(seg * (i + 1));
            float ly = cv.height/2 + rl * cv.sin(seg * (i + 1));
            cv.line(x, y, lx, ly);
        }
        cv.background(0);
        UIEventManager.renderElements(cv);
    }

    @Override
    public void activate() {

    }
}