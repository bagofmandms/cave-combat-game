package game;

import game.interactables.UIEventManager;
import game.renders.Renderers;
import game.resourcemanagement.ResourceManager;
import networking.client.ClientNetworkingManager;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

public class Canvas extends PApplet {

    ClientNetworkingManager test2;
    Scanner sc;

    public void settings(){
        fullScreen();
    }

    public void setup() {
        frameRate(60);

        UIEventManager.findScale(this);

        loadImages();

        UIEventManager.setActiveRenderer(Renderers.TITLE_SCREEN);

        textAlign(CENTER, CENTER);

        textFont(createFont("src/resources/ufonts.com_ebrima.ttf", 32));

    }

    public void draw(){
        UIEventManager.render(this);
    }

    public void mousePressed(){
        UIEventManager.mouseEvent(0, this);
    }

    public void mouseClicked(){
        UIEventManager.mouseEvent(2, this);
    }

    public void mouseReleased(){
        UIEventManager.mouseEvent(1, this);
    }

    public void keyPressed() {
        UIEventManager.keyEvent(this);
        if(key==27) {
            // UNCOMMENT IN FINAL BUILD
            //key = 0;
        }
    }

    public void loadImages(){
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("src/game/resourcemanagement/resources.txt")));
            String line;
            while ((line = br.readLine()) != null) {
                try{
                    PImage temp = loadImage("src/resources/" + line);
                    temp.resize((int)(temp.width * UIEventManager.scale), (int)(temp.height * UIEventManager.scale));
                    ResourceManager.images.add(temp);
                } catch (Exception e) {
                    Logger.WRITE("resource loader", e.toString());
                }
            }
        }catch (Exception e){
            Logger.WRITE("resource loader", e.toString());
        }
    }

}
