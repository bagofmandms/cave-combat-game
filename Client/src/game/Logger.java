package game;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.time.Instant;

public class Logger {
    public static final Logger LOGGER = new Logger();
    private Logger(){
        INITLOGGER(".");
    }
    private static FileOutputStream writer;
    private static void INITLOGGER(String outLocation){
        try{
            PrintWriter preWriter = new PrintWriter(outLocation + "/latest.txt");
            preWriter.print("");
            preWriter.close();
            File log = new File(outLocation + "/latest.txt");
            log.createNewFile();
            writer = new FileOutputStream(outLocation + "/latest.txt", true);
            WRITE("main.Logger", "Initialized at " + Instant.now());
        }catch(Exception e){
            System.out.println("main.Logger error! " + e);
        }
    }
    public static void WRITE(String id, String message){
        System.out.println("[" + id + "] : " + message);
        try {
            writer.write(("[" + id + "] : " + message + "\n").getBytes());
        }catch(Exception e){
            System.out.println("main.Logger failed! " + e);
        }
    }
}
