package game.resourcemanagement;

import processing.core.PImage;

import java.util.ArrayList;

public class ResourceManager {
    public static ArrayList<PImage> images = new ArrayList<>();

    // Loading function found in canvas because Processing is dumb

    public enum TEXTURES{
        CREATE_ROOM_BUTTON,
        CREATE_ROOM_BUTTON_PRESSED,
        EXIT_BUTTON,
        EXIT_BUTTON_PRESSED,
        JOIN_ROOM_BUTTON,
        JOIN_ROOM_BUTTON_PRESSED,
        SETTINGS_BUTTON,
        SETTINGS_BUTTON_PRESSED,
        TEST;

    }

    public static PImage getImage(TEXTURES name){
        return images.get(name.ordinal());
    }
}
