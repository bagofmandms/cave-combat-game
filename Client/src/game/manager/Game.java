package game.manager;

import game.Logger;
import game.interactables.UIEvent;
import networking.client.ClientNetworkingManager;
import networking.server.Server;

public class Game {
    private static Server server;
    private static ClientNetworkingManager clientNetworkingManager;
    public static void ManageUIEvent(UIEvent event){
        System.out.println("test");
    }
    public static void OpenServer(int size, int port, String pass){
        Logger.WRITE("Server init", "starting server:\n\t|port : " + port + "\n\t|size : " + size + "\n\t|pass : " + pass);
        server = new Server(size, port, pass);
        server.open();
    }
    public static void StartClient(String IP, int port, String pass){
        clientNetworkingManager = new ClientNetworkingManager();
        Logger.WRITE("Client init", "starting client networking manager:\n\t|ip : " + IP + "\n\t|port : " + port + "\n\t|pass : " + pass);
        clientNetworkingManager.connect(IP, port, pass);
    }
}
