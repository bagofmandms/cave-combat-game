package networking.client;

import game.Logger;
import networking.common.EndpointManager;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class ClientNetworkingManager {
    public EndpointManager manager;
    public int connect(String ip, int port, String password){
        Logger.WRITE("Client Networking Controller", "Attempting connection to " + ip);
        try{
            Socket tempSocket = new Socket(ip, port);
            DataInputStream temp_in = new DataInputStream(tempSocket.getInputStream());
            DataOutputStream temp_out = new DataOutputStream(tempSocket.getOutputStream());
            temp_out.writeUTF(password);
            temp_out.flush();
            int response = Integer.parseInt(temp_in.readUTF());
            if(response == -1){
                Logger.WRITE("Client Networking Controller", "Server is full.");
                return -1;
            }else if(response == -2){
                Logger.WRITE("Client Networking Controller", "Password declined.");
                return -2;
            }else{
                Logger.WRITE("Client Networking Controller", "Password accepted, connecting to assigned port");
                Socket socket = new Socket(ip, response);
                manager = new EndpointManager(socket, 0);
            }
        }catch (Exception e){
            Logger.WRITE("Client Networking Controller", "Failed to connect to " + ip);
            return 0;
        }
        return 0;
    }
}
