package networking.server;

import game.Logger;
import networking.common.EndpointManager;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class RegistryThread extends Thread {
    private Server parent;
    public RegistryThread(Server parent){
        this.parent = parent;
    }

    @Override
    public void run() {
        Logger.WRITE("Registry Thread", "Client listener initiated");
        super.run();
        try{
            while (true) {
                ServerSocket serverSocket = new ServerSocket(parent.getPort());
                Socket socket = serverSocket.accept();
                serverSocket.close();
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                DataInputStream in = new DataInputStream(socket.getInputStream());
                if(parent.getPassword().equals(in.readUTF())) {
                    int slot = parent.getClientSlot();
                    if (slot != -1) {
                        Logger.WRITE("Registry Thread", "Client accepted on slot: " + slot);
                        serverSocket = new ServerSocket(parent.getPort() + slot + 1);
                        out.writeUTF(String.valueOf(parent.getPort() + slot + 1));
                        out.flush();
                        socket.close();
                        socket = serverSocket.accept();
                        serverSocket.close();
                        parent.clients[slot] = new EndpointManager(socket, slot, parent.getGameManager());
                    } else {
                        Logger.WRITE("Registry Thread", "Client declined (Server full)");
                        out.writeUTF("-1");
                        out.flush();
                        socket.close();
                    }
                }else{
                    Logger.WRITE("Registry Thread", "Client declined (Wrong password)");
                    out.writeUTF("-2");
                    out.flush();
                    socket.close();
                }
            }
        }catch (Exception e){
            Logger.WRITE("Registry Thread", "Thread failed: " + e);
        }
    }
}
