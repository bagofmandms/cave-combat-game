package networking.server;

import game.Logger;

public class ServerThread extends Thread {
    private Server server;
    public void preConfig(int size, int port, String password){
        server = new Server(size, port, password);
    }
    @Override
    public void run() {
        super.run();
        Logger.WRITE("ServerThread", "Started server thread (in-client)");
        server.open();
    }
}
