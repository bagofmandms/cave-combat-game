package networking.server;

import game.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class DedicatedServerLauncher {
    public static void main(String[] args){
        Logger.WRITE("Dedicated Server init", "Launching server . . .");
        int size = 1;
        int port = 8080;
        String password = "";
        try{
            File file = new File("./config.txt");
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            size = Integer.parseInt(line.substring(line.indexOf(":") + 1));
            line = br.readLine();
            port = Integer.parseInt(line.substring(line.indexOf(":") + 1));
            line = br.readLine();
            password = line.substring(line.indexOf(":") + 1);
        }catch (Exception e){
            Logger.WRITE("Dedicated Server init", "Failed to read config file: " + e);
        }
        Logger.WRITE("Dedicated Server init", "starting server:\n\t|port : " + port + "\n\t|size : " + size + "\n\t|pass : " + password);
        Server server = new Server(size, port, password);
        server.open();
    }
}
