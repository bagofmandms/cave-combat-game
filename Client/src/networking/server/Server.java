package networking.server;

import networking.common.EndpointManager;

public class Server {
    private int size;
    private int port;
    private String password;
    private RegistryThread registry;
    protected EndpointManager[] clients;
    private GameManager gameManager;

    public Server(int size, int port, String password){
        this.size = size;
        this.port = port;
        this.password = password;
        gameManager = new GameManager(this);
        clients = new EndpointManager[size];
        for(int i = 0; i < size; i++){
            clients[i] = null;
        }
        registry = new RegistryThread(this);
    }

    public void open(){
        registry.start();
    }
    protected int getClientSlot(){
        for(int i = 0; i < size; i++){
            if(clients[i] == null){
                return i;
            }
        }
        return -1;
    }
    protected int getPort(){
        return port;
    }
    protected String getPassword(){
        return password;
    }

    public GameManager getGameManager() {
        return gameManager;
    }
}
