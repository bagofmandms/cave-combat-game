package networking.server;

import networking.common.EndpointManager;

public class GameManager {
    Server parent;
    public GameManager(Server parent){
        this.parent = parent;
    }
    public void HandleEvent(String data, int id){
        for(EndpointManager client : parent.clients){
            if(client != null) {
                client.send(id + " : " + data);
            }
        }
    }
}
