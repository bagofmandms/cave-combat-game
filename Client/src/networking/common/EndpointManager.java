package networking.common;

import game.Logger;
import networking.common.EndpointThread;
import networking.server.GameManager;

import java.io.DataOutputStream;
import java.net.Socket;

public class EndpointManager {
    private EndpointThread thread;
    protected int id;
    private GameManager game;
    protected Socket socket;
    private DataOutputStream out;
    public EndpointManager(Socket socket, int slot, GameManager gameManager) {
        thread = new EndpointThread(this, socket);
        this.socket = socket;
        id = slot;
        game = gameManager;
        try {
            out = new DataOutputStream(socket.getOutputStream());
        }catch (Exception e){
            Logger.WRITE("Endpoint " + id, "Endpoint failed in out stream init: " + e);
        }
        thread.start();
    }
    public EndpointManager(Socket socket, int slot) {
        thread = new EndpointThread(this, socket);
        this.socket = socket;
        id = slot;
        game = null;
        try {
            out = new DataOutputStream(socket.getOutputStream());
        }catch (Exception e){
            Logger.WRITE("Endpoint " + id, "Endpoint failed in out stream init: " + e);
        }
        thread.start();
    }
    void onReceive(String data){
        Logger.WRITE("Temp Debug (Endpoint log) " + id, data);
        if(game != null){
            sendGameEvent(data);
        }
    }
    public void send(String data){
        try {
            out.writeUTF(data);
            out.flush();
        }catch (Exception e){
            Logger.WRITE("Endpoint " + id, "Endpoint failed in message send: " + e);
        }
    }
    private void sendGameEvent(String data){
        game.HandleEvent(data, id);
    }
}
