package networking.common;

import game.Logger;

import java.io.DataInputStream;
import java.net.Socket;

public class EndpointThread extends Thread {
    private EndpointManager parent;
    private DataInputStream in;
    public EndpointThread(EndpointManager parent, Socket socket){
        this.parent = parent;
        try{
            in = new DataInputStream(socket.getInputStream());
        }catch (Exception e){
            Logger.WRITE("Endpoint " + parent.id, "Endpoint failed in listener init: " + e);
        }
    }
    @Override
    public void run() {
        super.run();
        try {
            while (true) {
                parent.onReceive(in.readUTF());
            }
        } catch (Exception e){
            Logger.WRITE("Endpoint " + parent.id, "Endpoint dropped: " + e);
        }
    }
}
