import primary.Server;
import registry.SocketRegistry;

public class Test {
    public static void main(String[] args){
        Server server = new Server(8);
        server.addRegistry(new SocketRegistry(server, 48900));
        server.openServer();
    }
}
