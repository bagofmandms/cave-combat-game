package primary;

public abstract class GameManager extends Thread {
    public abstract void handleEvent(int id, String data);
    public void open(){
        this.start();
    }
    public void close(){

    }

    @Override
    public void run() {
        super.run();

    }
}
