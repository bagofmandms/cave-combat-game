package primary;

import registry.Registry;
import tunnels.Tunnel;

import java.util.ArrayList;

public class Server {
    private int size;
    private ArrayList<Registry> registries = new ArrayList<>();
    private boolean open = false;
    private GameManager gameManager;
    private Tunnel[] clients;
    public Server(int size){
        this.size = size;
        clients = new Tunnel[size];
        for(Tunnel client : clients){
            client = null;
        }
    }

    public int getSize() {
        return size;
    }

    public void openServer(){
        open = true;
        for(Registry reg : registries){
            reg.open();
        }
        gameManager.open();
    }

    public void closeServer(){
        open = false;
        for(Registry reg : registries){
            reg.close();
        }
        for(int i = 0; i < clients.length; i++){
            clients[i].close();
        }
        gameManager.close();
    }

    public Tunnel[] getClientsList(){
        return clients;
    }

    public void addRegistry(Registry registry){
        registries.add(registry);
    }

    public void setGameManager(GameManager gameManager){
        this.gameManager = gameManager;
    }

    public int getClientSlot(){
        for(int i = 0; i < clients.length; i++){
            if(clients[i] != null){
                return i;
            }
        }
        return -1;
    }

    public GameManager getGameManager(){
        return gameManager;
    }
}
