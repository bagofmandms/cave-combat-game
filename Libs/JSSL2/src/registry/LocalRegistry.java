package registry;

import primary.Server;

public class LocalRegistry extends Registry {
    public LocalRegistry(Server parent){
        super(parent);
    }

    @Override
    public void open() {

    }

    @Override
    public void close() {

    }
}
