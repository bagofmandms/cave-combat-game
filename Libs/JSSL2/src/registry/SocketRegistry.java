package registry;

import primary.Server;
import threads.SocketRegistryThread;
import tunnels.Tunnel;

public class SocketRegistry extends Registry {

    private SocketRegistryThread thread;
    private int registryPort;

    public SocketRegistry(Server parent, int registryPort){
        super(parent);
        this.registryPort = registryPort;
    }

    @Override
    public void open() {
        thread = new SocketRegistryThread(registryPort, this);
        thread.start();
    }

    @Override
    public void close() {
        thread = null;
    }
}
