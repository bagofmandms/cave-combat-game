package registry;

import primary.Server;
import tunnels.Tunnel;

public abstract class Registry {
    public Server parent;
    public Registry(Server parent){
        this.parent = parent;
    }
    public abstract void open();
    public abstract void close();
}
