package threads;

import tunnels.SocketTunnel;
import utils.Logger;

import java.io.DataInputStream;

public class SocketTunnelThread extends Thread {
    DataInputStream in;
    SocketTunnel parent;
    public SocketTunnelThread(DataInputStream in, SocketTunnel parent){
        this.in = in;
        this.parent = parent;
    }

    @Override
    public void run() {
        super.run();
        while(true){
            try{
                parent.onRecieve(in.readUTF());
            }catch (Exception e){
                Logger.log(String.valueOf(parent.id), "Client dropped for reason : " + e);
            }
        }
    }
}
