package threads;

import registry.SocketRegistry;
import tunnels.SocketTunnel;
import utils.Logger;

import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketRegistryThread<client extends SocketTunnel> extends Thread {
    private int registryPort;
    private SocketRegistry parent;
    ServerSocket serverSocket;
    Class<SocketTunnel> clientType;
    public SocketRegistryThread(int registryPort, SocketRegistry parent, Class<SocketTunnel> clientType){
        this.registryPort = registryPort;
        this.parent = parent;
        this.clientType = clientType;
    }

    @Override
    public void run() {
        super.run();
        try {
            while (this.isAlive()) {
                serverSocket = new ServerSocket(registryPort);
                Socket socket = serverSocket.accept();
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                int slot = parent.parent.getClientSlot();
                if(slot != -1){
                    serverSocket = new ServerSocket(registryPort + slot);
                    out.writeUTF(String.valueOf(registryPort + slot + 1));
                    out.flush();
                    socket = serverSocket.accept();
                    parent.parent.getClientsList()[slot] = clientType.newInstance();
                    ((client)parent.parent.getClientsList()[slot]).initialize(slot, parent.parent.getGameManager(), socket);
                }else {
                    out.writeUTF("-1");
                    out.flush();
                }
            }
        }catch (Exception e){
            Logger.log("Socket Registry Thread", "registry listener failed");
        }
    }
}
