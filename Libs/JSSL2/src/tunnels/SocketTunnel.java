package tunnels;

import primary.GameManager;
import threads.SocketTunnelThread;
import utils.Logger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public abstract class SocketTunnel extends Tunnel{
    Socket socket;
    protected DataInputStream in;
    DataOutputStream out;
    SocketTunnelThread thread;
    public SocketTunnel(){
        super();
    }

    public void initialize(int id, GameManager game, Socket socket){
        this.socket = socket;
        this.id = id;
        this.gameRef = game;
        try {
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
        }catch (Exception e){
            Logger.log("Socket tunnel " + id, "failed to generate data streams");
        }
        thread = new SocketTunnelThread(in, this);
    }

    @Override
    public abstract void close();

    public abstract void onRecieve(String data);

    @Override
    public void send(String data) {
        try {
            out.writeUTF(data);
            out.flush();
        }catch (Exception e){
            Logger.log("Socket tunnel " + id, "failed to send data");
        }
    }
}
