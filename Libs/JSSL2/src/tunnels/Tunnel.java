package tunnels;


import primary.GameManager;

public abstract class Tunnel {
    public int id;
    protected GameManager gameRef;
    public abstract void initialize(int id, GameManager gameRef);
    public Tunnel(){

    }
    public abstract void close();
    public abstract void onRecieve(String data);
    public abstract void send(String data);
    public void processEvent(String data){
        gameRef.handleEvent(id, data);
    }
}
