package bagofmandms.main.networking;

import bagofmandms.main.logging.Loggable;
import bagofmandms.main.logging.Logger;
import bagofmandms.main.networking.events.GameEvent;

public abstract class GameController extends Loggable {
    /**
     * Used to control the game and handle general or specific events related to the game.
     */
    private Server parent;
    public GameController(Logger logger, Server parent){
        super("GameController", logger);
        this.parent = parent;
        gameInit();
        log("Game controller initialized");
    }

    /**
     * Called when a client joins on a given port. Non-specific to port.
     * Should be used for non-specific initialization only.
     * @param Port
     * Port that client connected to.
     */
    public abstract void joinEvent(int Port);

    /**
     * Called when a client disconnects
     * @param port
     * Port of disconnected client.
     */
    public abstract void leaveEvent(int port);

    /**
     * Called when the game is initialized for client independent initialization.
     */
    public abstract void gameInit();

    /**
     * Handles events passed by client controllers.
     * @param event
     * The event which contains the port of the given client and the associated data.
     */
    public abstract void gameEventReceived(GameEvent event);

    /**
     * The method called when the server is closed.
     * Should be used to store data and end processes.
     */
    public abstract void close();

    /**
     * Used to send data to a given client by port.
     * @param data
     * Data to send as string.
     * @param clientPort
     * Port of targeted client.
     */
    public void send(String data, int clientPort){
        parent.send(data, clientPort);
    }
}
