package bagofmandms.main.networking;

import bagofmandms.main.logging.Loggable;
import bagofmandms.main.logging.Logger;

public abstract class Server extends Loggable{
    /**
     * Object which contains all other server related object.
     * Abstract so the Client controller can be abstracted.
     */
    private Logger serverLog;
    private boolean[] clientRegistry;
    private int size;
    private String password;
    private Client[] clients;
    private int regPort;
    private RegistryHandler regHandler;
    public GameController gameController;
    public Server(Logger logger, int size, int regPort, String password, GameController game){
        super("Server", logger);
        serverLog = logger;
        this.size = size;
        this.regPort = regPort;
        this.password = password;
        this.clients = new Client[size];
        this.gameController = game;
        clientRegistry = new boolean[size];
    }
    protected String getClientPort(){
        for(int i = 0; i < clientRegistry.length; i++){
            if(!clientRegistry[i]){
                clientRegistry[i] = true;
                gameController.joinEvent(i + regPort + 1);
                clients[i].start();
                return Integer.toString(clients[i].getPort());
            }
        }
        log("client declined, server is full");
        return "0";
    }

    /**
     * Opens the server and prepares the clients to open ports.
     */
    public void open(){
        regHandler = new RegistryHandler(serverLog);
        regHandler.setPort(regPort);
        regHandler.setPassword(password);
        for(int i = 0; i < clients.length; i++){
            clients[i] = new Client(regPort + i + 1, this);
            clients[i].init(getController(serverLog, regPort + i + 1, clients[i]));
        }
        regHandler.open();
        log("Opened.");
    }

    /**
     * Closes registry port and disconnects all users with notifications "Server closed".
     */
    public void close(){
        regHandler.close();
        for (Client client : clients) {
            if(client != null && client.controller != null) {
                client.disconnect("Server closed");
            }
        }
    }

    /**
     * Closes and reopens the server.
     */
    public void restart(){
        log("Server restarting...");
        gameController.close();
        close();
        open();
        log("Restart complete.");
    }
    protected void restartClient(int port){
        for(int i = 0; i < size; i++){
            if(clients[i].getPort() == port){
                clients[i] = new Client(clients[i].getPort(), this);
                clients[i].init(getController(serverLog, regPort + i + 1, clients[i]));
                clientRegistry[i] = false;
            }
        }
        log("Restarted client slot on port " + port + " .");
    }

    /**
     * Used to generate instances of abstracted client controller.
     * Should return a default abstracted client controller object to be passed to nonspecific clients.
     * @param logger
     * Logger reference to be passed into the default constructor of client controller.
     * @param parent
     * Client reference to be passed into the default constructor of client controller.
     * @param port
     * Port value to be passed into the default constructor of client controller.
     * @return
     * Returns the generated client controller.
     */
    protected abstract ClientController getController(Logger logger, int port, Client parent);
    public void send(String data, int port){
        for(int i = 0; i < size; i++){
            if(clients[i].getPort() == port){
                clients[i].send(data);
            }
        }
    }
    protected void leaveEventReceived(int port){
        gameController.leaveEvent(port);
    }
}
