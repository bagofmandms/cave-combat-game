package bagofmandms.main.networking;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class RegistryThread extends Thread {
    /**
     * Thread to handle client connections.
     * Should be left alone.
     */
    private boolean running;
    private RegistryHandler parent;
    public RegistryThread(RegistryHandler parent) {
        this.parent = parent;
    }
    public void setRunning(){
        running = true;
        this.start();
    }
    public void stopRunning(){
        running = false;
    }
    @Override
    public void run() {
        super.run();
        ServerSocket connection;
        try{
            connection = new ServerSocket(parent.port);
            while(running){
                try {
                    Socket socket = connection.accept();
                    parent.passLog("join request from " + socket.getRemoteSocketAddress().toString());
                    DataInputStream in = new DataInputStream(socket.getInputStream());
                    DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                    if(in.readUTF().equals(parent.password)){
                        parent.passLog("password accepted from " + socket.getRemoteSocketAddress().toString());
                        out.writeUTF(parent.callGetClientPort());
                        out.flush();
                    }else {
                        parent.passLog("password declined from " + socket.getRemoteSocketAddress().toString());
                        out.writeUTF("-1");
                        out.flush();
                    }
                    socket.close();
                }catch(Exception e){
                    parent.passLog("error on thread: " + e);
                }
            }
        }catch(Exception e){
            parent.passLog("error on thread: " + e);
        }
        parent.passLog("Registry port closed");
    }
}
