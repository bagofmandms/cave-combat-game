package bagofmandms.main.networking;

import bagofmandms.main.networking.events.GameEvent;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Client extends Thread {
    /**
     * Contains actual networking systems.
     * Handles interaction between external objects and client socket.
     */
    private int port;
    private boolean running;
    private ServerSocket serverSocket;
    private Socket socket;
    private DataOutputStream out;
    private DataInputStream in;
    protected ClientController controller;
    private Server parent;
    public Client(int port, Server parent){
        this.port = port;
        this.parent = parent;
    }

    /**
     * Returns the port of the client
     * @return
     * client port.
     */
    public int getPort(){
        return port;
    }

    /**
     * Sends data to the user as a string.
     * @param data
     * The string which is sent.
     */
    protected void send(String data){
        try {
            out.writeUTF(data);
            out.flush();
        }catch(Exception e){
            controller.passLog("client message send failed: " + e);
        }
    }
    public void init(ClientController controller) {
        this.controller = controller;
        try {
            controller.passLog("client initializing");
            serverSocket = new ServerSocket(port);
        }catch(Exception e){
            controller.passLog("client initialization failed: " + e);
        }
    }

    /**
     * Disconnects user after sending a disconnect notifier to the client.
     * @param reason
     * Disconnection notifier.
     */
    public void disconnect(String reason){
        send("term:" + reason);
        controller.passLog("client disconnected for " + reason);
        running = false;
    }
    public void run() {
        try{
            controller.passLog("client running");
            socket = serverSocket.accept();
            controller.passLog("client connected: " + socket.getRemoteSocketAddress().toString());
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
            running = true;
            while(running){
                try {
                    controller.MessageReceivedEvent(in.readUTF());
                }catch(Exception e){
                    controller.passLog("client disconnected: " + e);
                    serverSocket.close();
                    running = false;
                    controller.Restart();
                    parent.restartClient(port);
                    parent.leaveEventReceived(getPort());
                    return;
                }
            }
        }catch (Exception e){
            System.out.println("client connection failed: " + e);
        }
    }
    protected void passGameEvent(GameEvent event){
        parent.gameController.gameEventReceived(event);
    }
}
