package game;

import bagofmandms.main.logging.Logger;
import bagofmandms.main.networking.GameController;
import bagofmandms.main.networking.Server;
import bagofmandms.main.networking.events.GameEvent;
import game.gameObjects.Event;
import game.gameObjects.GameThread;
import game.gameObjects.Player;

public class GameImplementation extends GameController {
    GameThread gameThread;
    Player[] players;
    public GameImplementation(Logger logger, Server parent, int size){
        super(logger, parent);
        players = new Player[size];
        for(Player player : players){
            player = new Player();
        }
        gameThread = new GameThread(this, size, 20, 20);
    }

    @Override
    public void gameInit() {
        log("game initialized");
    }

    /* Protocol:
     * x:y:d
     * d:
     * 0 = Up
     * 1 = Right
     * 2 = Down
     * 3 = Left
     */
    @Override
    public void gameEventReceived(GameEvent gameEvent) {
        if(playerAlive(getTeam(Integer.valueOf(gameEvent.caller)))){
            gameThread.addEvent(new Event(gameEvent.data), getTeam(Integer.valueOf(gameEvent.caller)));
            sendToAllBut(gameEvent.data, Integer.valueOf(gameEvent.caller));
        }
    }

    public void kill(int team){
        log("killed player " + team);
        players[team] = new Player();
    }

    @Override
    public void close() {

    }

    @Override
    public void joinEvent(int port) {
        send(gameThread.getMap(), port);
        if(generatePlayer(port)){
            gameThread.start();
        }
    }

    @Override
    public void leaveEvent(int port) {
        for (Player player : players) {
            if(player.isPlayer(port)){
                player = new Player();
            }
        }
    }

    public void passLog(String data){
        log(data);
    }

    private boolean generatePlayer(int port){
        log("player generated for " + port + ". Team: " + getTeam(port));
        for(int i = 0; i < players.length; i++){
            if(players[i].isActive()){
                players[i] = new Player(port, i);
                if(i == players.length - 1){
                    log("game starting!");
                    return true;
                }
                break;
            }
        }
        return false;
    }

    private int getTeam(int port){
        for(Player player : players){
            if(player.isPlayer(port)){
                return player.getTeam();
            }
        }
        return 0;
    }

    public boolean playerAlive(int team){
        return players[team].isActive();
    }

    private void sendToAllBut(String data, int port){
        for(Player player : players){
            if(!player.isPlayer(port)){
                send(data, player.getPort());
            }
        }
    }
}