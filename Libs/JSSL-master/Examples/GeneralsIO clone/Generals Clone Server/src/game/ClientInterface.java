package game;

import bagofmandms.main.logging.Logger;
import bagofmandms.main.networking.Client;
import bagofmandms.main.networking.ClientController;

public class ClientInterface extends ClientController {
    public ClientInterface(Logger logger, int port, Client parent){
        super(logger, port, parent);
    }

    @Override
    public void Restart() {

    }

    @Override
    public void MessageReceivedEvent(String s) {
        sendGameEvent(s);
    }
}
