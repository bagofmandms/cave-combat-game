package game.gameObjects;

public class Event {
    public int x;
    public int y;
    public int d;
    public Event(String data){
        String[] parse = data.split(":");
        x = Integer.valueOf(parse[0]);
        y = Integer.valueOf(parse[1]);
        d = Integer.valueOf(parse[2]);
    }
}
